<?php

require __DIR__.'/vendor/autoload.php';

use Symfony\Component\Console\Application;

use VKPhotoParser\Command\GetStatCommand;
use VKPhotoParser\Command\GetUserPhotosAndAlbumsCommand;
use VKPhotoParser\Command\GetUserStatCommand;
use VKPhotoParser\Command\ProcessUserCommand;
use VKPhotoParser\Command\SaveAlbumAndPublishPhotosCommand;
use VKPhotoParser\Command\SavePhotoCommand;
use VKPhotoParser\Command\SaveUserAndPublishAlbumsCommand;
use VKPhotoParser\Container;

$parameters = require __DIR__.'/parameters.php';
$container = new Container($parameters);

$application = new Application();
$application->add(new GetStatCommand($container->getUserRepository()));
$application->add(new GetUserStatCommand(
    $container->getAlbumRepository(),
    $container->getPhotoRepository(),
    $container->getUserRepository(),
    $container->getVkApi()
));
$application->add(new ProcessUserCommand(
    $container->getUserRepository(),
    $container->getVkApi(),
    $container->getUserProducer()
));

$application->add(new SaveUserAndPublishAlbumsCommand($container->getUserConsumer()));
$application->add(new SaveAlbumAndPublishPhotosCommand($container->getAlbumConsumer()));
$application->add(new SavePhotoCommand($container->getPhotoConsumer()));

$application->run();
