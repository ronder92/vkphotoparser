<?php

namespace VKPhotoParser\Storage;

interface StorageInterface
{
    /**
     * @param string $query
     * @param array $parameters
     * @return mixed
     */
    public function fetchAll($query, $parameters = []);

    /**
     * @param string $query
     * @param array $parameters
     * @return mixed
     */
    public function count($query, $parameters = []);

    /**
     * @param string $query
     * @param array $parameters
     * @return mixed
     */
    public function insert($query, $parameters = []);
}