<?php

namespace VKPhotoParser\Storage;

use PDO;
use PDOException;

class MysqlStorage implements StorageInterface
{
    /**
     * @var string
     */
    private $host;

    /**
     * @var string
     */
    private $user;

    /**
     * @var string
     */
    private $password;

    /**
     * @var string
     */
    private $dbname;

    /**
     * @var PDO
     */
    private $dbh;

    /**
     * MysqlStorage constructor.
     * @param string $host
     * @param string $user
     * @param string $password
     * @param string $dbname
     */
    public function __construct($host, $user, $password, $dbname)
    {
        $this->host = $host;
        $this->user = $user;
        $this->password = $password;
        $this->dbname = $dbname;
    }

    private function connect()
    {
        if (null !== $this->dbh) {
            return;
        }

        $dsn = 'mysql:dbname='. $this->dbname .';host=' . $this->host;

        try {
            $this->dbh = new PDO($dsn, $this->user, $this->password, [PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION]);
        } catch (PDOException $e) {
            // TODO: add to log
        }
    }

    /**
     * @inheritdoc
     */
    public function fetchAll($query, $parameters = [])
    {
        $sth = $this->prepareAndExecuteQuery($query, $parameters);

        return $sth->fetchAll(PDO::FETCH_ASSOC);
    }

    /**
     * @param string $query
     * @param array $parameters
     * @return int
     */
    public function count($query, $parameters = [])
    {
        $sth = $this->prepareAndExecuteQuery($query, $parameters);

        return $sth->fetchColumn();
    }

    /**
     * @inheritdoc
     */
    public function insert($query, $parameters = [])
    {
        $this->prepareAndExecuteQuery($query, $parameters);
    }

    /**
     * @param string $query
     * @param array $parameters
     * @return \PDOStatement
     */
    private function prepareAndExecuteQuery($query, $parameters = [])
    {
        $this->connect();

        $sth = $this->dbh->prepare($query);

        foreach ($parameters as $parameterName => $parameterValue) {
            $sth->bindValue(
                $parameterName,
                $parameterValue,
                is_numeric($parameterValue) ? PDO::PARAM_INT : PDO::PARAM_STR
            );
        }

        $sth->execute();

        return $sth;
    }
}