<?php

namespace VKPhotoParser;

interface HttpClientInterface
{
    /**
     * @param string $url
     * @return mixed
     */
    public function sendRequestAndReturnResponse($url);
}
