<?php

namespace VKPhotoParser\Amqp;

use PhpAmqpLib\Channel\AMQPChannel;
use PhpAmqpLib\Connection\AMQPStreamConnection;

class AmqpConnection
{
    /**
     * @var string
     */
    private $host;

    /**
     * @var int
     */
    private $port;

    /**
     * @var string
     */
    private $user;

    /**
     * @var string
     */
    private $password;

    /**
     * @var AMQPStreamConnection
     */
    protected $amqpConnection;

    /**
     * @var AMQPChannel
     */
    protected $amqpChannel;

    /**
     * @param string $host
     * @param int $port
     * @param string $user
     * @param string $password
     */
    public function __construct($host, $port, $user, $password)
    {
        $this->host = $host;
        $this->port = $port;
        $this->user = $user;
        $this->password = $password;
    }

    public function __destruct()
    {
        if (null !== $this->amqpChannel) {
            $this->amqpChannel->close();
        }

        if (null !== $this->amqpConnection) {
            $this->amqpConnection->close();
        }
    }
    
    /**
     * @return AMQPChannel
     */
    public function getAmqpChannel()
    {
        if (null !== $this->amqpChannel) {
            return $this->amqpChannel;
        }

        $this->amqpConnection = new AMQPStreamConnection($this->host, $this->port, $this->user, $this->password);
        $this->amqpChannel = $this->amqpConnection->channel();

        return $this->amqpChannel;
    }

    /**
     * @param Exchange $exchange
     * @param Queue $queue
     */
    public function declareExchangeAndQueue(Exchange $exchange, Queue $queue)
    {
        $amqpChannel = $this->getAmqpChannel();

        $amqpChannel->exchange_declare($exchange, 'direct', false, true, false);
        $amqpChannel->queue_declare($queue, false, true, false, false);
        $amqpChannel->queue_bind($queue, $exchange);
    }
}
