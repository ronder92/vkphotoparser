<?php

namespace VKPhotoParser\Amqp;

abstract class BaseAmqp
{
    /**
     * @var AmqpConnection
     */
    protected $amqpConnection;

    /**
     * @var Exchange
     */
    protected $exchange;

    /**
     * @var Queue
     */
    protected $queue;

    /**
     * @param AmqpConnection $amqpConnection
     * @param Exchange $exchange
     * @param Queue $queue
     */
    public function __construct(AmqpConnection $amqpConnection, Exchange $exchange, Queue $queue)
    {
        $this->amqpConnection = $amqpConnection;
        $this->exchange = $exchange;
        $this->queue = $queue;
    }
}
