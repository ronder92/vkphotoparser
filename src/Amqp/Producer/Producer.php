<?php

namespace VKPhotoParser\Amqp\Producer;

use PhpAmqpLib\Message\AMQPMessage;
use VKPhotoParser\Amqp\BaseAmqp;
use VKPhotoParser\Amqp\Producer\ProducerInterface;

class Producer extends BaseAmqp implements ProducerInterface
{
    /**
     * @param string $messageBody
     */
    public function publish($messageBody)
    {
        $this->amqpConnection->declareExchangeAndQueue($this->exchange, $this->queue);

        $this->amqpConnection->getAmqpChannel()->basic_publish(
            new AMQPMessage(
                json_encode($messageBody),
                $this->getDefaultMessageProperties()
            ),
            $this->exchange
        );
    }

    /**
     * @return array
     */
    protected function getDefaultMessageProperties()
    {
        return [
            'content_type' => 'text/plain',
            'delivery_mode' => AMQPMessage::DELIVERY_MODE_PERSISTENT
        ];
    }
}
