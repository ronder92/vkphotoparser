<?php

namespace VKPhotoParser\Amqp\Producer;

interface ProducerInterface
{
    /**
     * @param string $messageBody
     */
    public function publish($messageBody);
}
