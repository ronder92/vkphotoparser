<?php

namespace VKPhotoParser\Amqp\Consumer;

use PhpAmqpLib\Message\AMQPMessage;

interface ConsumerInterface
{
    /**
     * @param AMQPMessage $AMQPMessage
     * @return mixed
     */
    public function execute(AMQPMessage $AMQPMessage);
}
