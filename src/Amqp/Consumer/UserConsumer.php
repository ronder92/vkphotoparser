<?php

namespace VKPhotoParser\Amqp\Consumer;

use PhpAmqpLib\Message\AMQPMessage;

use VKPhotoParser\Amqp\AmqpConnection;
use VKPhotoParser\Amqp\Exchange;
use VKPhotoParser\Amqp\Producer\Producer;
use VKPhotoParser\Amqp\Queue;
use VKPhotoParser\Repository\UserRepository;
use VKPhotoParser\VkApi;

class UserConsumer extends BaseConsumer implements ConsumerInterface
{
    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * @var VkApi
     */
    private $vkApi;

    /**
     * @var Producer
     */
    private $albumProducer;

    /**
     * @param AmqpConnection $amqpConnection
     * @param Exchange $exchange
     * @param Queue $queue
     * @param UserRepository $photoRepository
     * @param VkApi $vkApi
     * @param Producer $photoProducer
     */
    public function __construct(
        AmqpConnection $amqpConnection,
        Exchange $exchange,
        Queue $queue,
        UserRepository $photoRepository,
        VkApi $vkApi,
        Producer $photoProducer
    ) {
        $this->userRepository = $photoRepository;
        $this->vkApi = $vkApi;
        $this->albumProducer = $photoProducer;

        parent::__construct($amqpConnection, $exchange, $queue);
    }

    /**
     * @param AMQPMessage $AMQPMessage
     * @return mixed
     */
    public function execute(AMQPMessage $AMQPMessage)
    {
        $AMQPMessageJsonDecoded = json_decode($AMQPMessage->getBody(), true);
        $userId = $AMQPMessageJsonDecoded['uid'];

        if ($this->userRepository->countByUserId($userId) == 0) {
            $this->userRepository->saveUser(
                $userId,
                $AMQPMessageJsonDecoded['first_name'],
                $AMQPMessageJsonDecoded['last_name']
            );
        }

        $userAlbums = $this->vkApi->getAlbumsByUserId($userId);

        foreach ($userAlbums as $userAlbum) {
            $this->albumProducer->publish([
                'aid' => $userAlbum['aid'],
                'owner_id' => $userAlbum['owner_id'],
                'title' => $userAlbum['title']
            ]);
        }

        return true;
    }
}
