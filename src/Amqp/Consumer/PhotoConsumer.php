<?php

namespace VKPhotoParser\Amqp\Consumer;

use PhpAmqpLib\Message\AMQPMessage;

use VKPhotoParser\Amqp\AmqpConnection;
use VKPhotoParser\Amqp\Exchange;
use VKPhotoParser\Amqp\Queue;
use VKPhotoParser\Repository\PhotoRepository;
use VKPhotoParser\VkApi;

class PhotoConsumer extends BaseConsumer implements ConsumerInterface
{
    /**
     * @var PhotoRepository
     */
    private $photoRepository;

    /**
     * @var VkApi
     */
    private $vkApi;

    /**
     * @param AmqpConnection $amqpConnection
     * @param Exchange $exchange
     * @param Queue $queue
     * @param PhotoRepository $photoRepository
     * @param VkApi $vkApi
     */
    public function __construct(
        AmqpConnection $amqpConnection,
        Exchange $exchange,
        Queue $queue,
        PhotoRepository $photoRepository,
        VkApi $vkApi
    ) {
        $this->photoRepository = $photoRepository;
        $this->vkApi = $vkApi;

        parent::__construct($amqpConnection, $exchange, $queue);
    }

    /**
     * @param AMQPMessage $AMQPMessage
     * @return mixed
     */
    public function execute(AMQPMessage $AMQPMessage)
    {
        $AMQPMessageJsonDecoded = json_decode($AMQPMessage->getBody(), true);
        $photoId = $AMQPMessageJsonDecoded['pid'];

        if ($this->photoRepository->countByPhotoId($photoId) > 0) {
            return true;
        }

        $firstPhoto = array_shift($AMQPMessageJsonDecoded['sizes']);

        $this->photoRepository->savePhoto(
            $photoId,
            $AMQPMessageJsonDecoded['aid'],
            $this->getPathToPhoto($firstPhoto['src'])
        );

        return true;
    }

    /**
     * @param string $urlToPhoto
     * @return string
     */
    private function getPathToPhoto($urlToPhoto)
    {
        return parse_url($urlToPhoto, PHP_URL_PATH);
    }
}
