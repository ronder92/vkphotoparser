<?php

namespace VKPhotoParser\Amqp\Consumer;

use PhpAmqpLib\Message\AMQPMessage;

use VKPhotoParser\Amqp\BaseAmqp;

abstract class BaseConsumer extends BaseAmqp
{
    /**
     * @var string
     */
    private $consumerTag;

    /**
     * @param AMQPMessage $AMQPMessage
     */
    private function acknowledgeMessage(AMQPMessage $AMQPMessage)
    {
        $AMQPMessage->delivery_info['channel']->basic_ack($AMQPMessage->delivery_info['delivery_tag']);
    }

    /**
     * @param AMQPMessage $AMQPMessage
     */
    private function rejectMessage(AMQPMessage $AMQPMessage)
    {
        $AMQPMessage->delivery_info['channel']->basic_reject($AMQPMessage->delivery_info['delivery_tag'], true);
    }

    /**
     * @param AMQPMessage $AMQPMessage
     */
    public function processMessage(AMQPMessage $AMQPMessage)
    {
        try {
            $consumerResponse = call_user_func([$this, 'execute'], $AMQPMessage);

            if ($consumerResponse === true) {
                $this->acknowledgeMessage($AMQPMessage);
            }
        } catch (\Exception $exception) {
            // TODO: add logger
            $this->rejectMessage($AMQPMessage);
        }
    }

    private function setupConsumer()
    {
        $this->amqpConnection->declareExchangeAndQueue($this->exchange, $this->queue);

        $this->amqpConnection->getAmqpChannel()->basic_consume(
            $this->queue,
            $this->consumerTag,
            false,
            false,
            false,
            false,
            [$this, 'processMessage']
        );
    }

    public function consume()
    {
        $AMQPChannel = $this->amqpConnection->getAmqpChannel();

        $this->setupConsumer();

        while (count($AMQPChannel->callbacks)) {
            $AMQPChannel->wait();
            $this->isStopConsumerNeeded();
        }
    }

    /**
     * @param string $consumerTag
     */
    public function setConsumerTag($consumerTag)
    {
        $this->consumerTag = $consumerTag;
    }

    private function isStopConsumerNeeded()
    {
        // TODO: implement some logic to stop consumer for prevent memory leak. Supervisor should re-run consumer
    }
}
