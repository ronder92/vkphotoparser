<?php

namespace VKPhotoParser\Amqp\Consumer;

use PhpAmqpLib\Message\AMQPMessage;

use VKPhotoParser\Amqp\AmqpConnection;
use VKPhotoParser\Amqp\Exchange;
use VKPhotoParser\Amqp\Producer\Producer;
use VKPhotoParser\Amqp\Queue;
use VKPhotoParser\Repository\AlbumRepository;
use VKPhotoParser\VkApi;

class AlbumConsumer extends BaseConsumer implements ConsumerInterface
{
    /**
     * @var AlbumRepository
     */
    private $albumRepository;

    /**
     * @var VkApi
     */
    private $vkApi;

    /**
     * @var Producer
     */
    private $photoProducer;

    /**
     * @param AmqpConnection $amqpConnection
     * @param Exchange $exchange
     * @param Queue $queue
     * @param AlbumRepository $albumRepository
     * @param VkApi $vkApi
     * @param Producer $photoProducer
     */
    public function __construct(
        AmqpConnection $amqpConnection,
        Exchange $exchange,
        Queue $queue,
        AlbumRepository $albumRepository,
        VkApi $vkApi,
        Producer $photoProducer
    ) {
        $this->albumRepository = $albumRepository;
        $this->vkApi = $vkApi;
        $this->photoProducer = $photoProducer;

        parent::__construct($amqpConnection, $exchange, $queue);
    }

    /**
     * @param AMQPMessage $AMQPMessage
     * @return mixed
     */
    public function execute(AMQPMessage $AMQPMessage)
    {
        $AMQPMessageJsonDecoded = json_decode($AMQPMessage->getBody(), true);
        $albumId = $AMQPMessageJsonDecoded['aid'];
        $userId = $AMQPMessageJsonDecoded['owner_id'];

        if ($this->albumRepository->countByAlbumId($albumId) == 0) {
            $this->albumRepository->saveAlbum($albumId, $userId, $AMQPMessageJsonDecoded['title']);
        }

        $albumPhotos = $this->vkApi->getPhotosByAlbumId($userId, $albumId);

        foreach ($albumPhotos as $albumPhoto) {
            $this->photoProducer->publish([
                'pid' => $albumPhoto['pid'],
                'aid' => $albumId,
                'sizes' => $albumPhoto['sizes']
            ]);
        }

        return true;
    }
}
