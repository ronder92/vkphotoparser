<?php

namespace VKPhotoParser\Repository;

class PhotoRepository extends AbstractRepository
{
    /**
     * @param int $photoId
     * @return int
     */
    public function countByPhotoId($photoId)
    {
        return $this->storage->count('SELECT COUNT(*) FROM `photo` WHERE `photo_id` = :photoId', [
            'photoId' => $photoId
        ]);
    }

    /**
     * @param int $userId
     * @return array
     */
    public function getPhotosWithAlbumsByUserId($userId)
    {
        return $this->storage->fetchAll(
            'SELECT p.photo_id, p.album_id, a.name, p.path FROM photo p
             INNER JOIN album a ON p.album_id = a.album_id
             WHERE a.user_id = :userId
             ORDER BY a.album_id',
            [
                ':userId' => $userId
            ]
        );
    }

    /**
     * @param int $photoId
     * @param int $albumId
     * @param string $path
     */
    public function savePhoto($photoId, $albumId, $path)
    {
        $this->storage->insert('INSERT INTO `photo` VALUES (:photoId, :albumId, :path)', [
            'photoId' => $photoId,
            'albumId' => $albumId,
            'path' => $path
        ]);
    }
}
