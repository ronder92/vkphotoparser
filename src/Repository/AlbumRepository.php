<?php

namespace VKPhotoParser\Repository;

class AlbumRepository extends AbstractRepository
{
    /**
     * @param int $albumId
     * @return int
     */
    public function countByAlbumId($albumId)
    {
        return $this->storage->count('SELECT COUNT(*) FROM `album` WHERE `album_id` = :albumId', [
            'albumId' => $albumId
        ]);
    }

    /**
     * @param int $userId
     * @return array
     */
    public function findByUserId($userId)
    {
        return $this->storage->fetchAll('SELECT album_id, name FROM album WHERE user_id = :userId ORDER BY album_id', [
            ':userId' => $userId
        ]);
    }

    /**
     * @param int $albumId
     * @param int $userId
     * @param string $albumName
     */
    public function saveAlbum($albumId, $userId, $albumName)
    {
        $this->storage->insert('INSERT INTO `album` VALUES (:albumId, :userId, :albumName)', [
            'albumId' => $albumId,
            'userId' => $userId,
            'albumName' => $albumName,
        ]);
    }
}
