<?php

namespace VKPhotoParser\Repository;

class UserRepository extends AbstractRepository
{
    /**
     * @param int $userId
     * @return int
     */
    public function countByUserId($userId)
    {
        return $this->storage->count('SELECT COUNT(*) FROM `user` WHERE `user_id` = :userId', [
            'userId' => $userId
        ]);
    }

    /**
     * @param int|null $offset
     * @param int|null $limit
     * @return array
     */
    public function findAllUserWithAlbumsAndPhotosCount($offset = null, $limit = null)
    {
        $parameters = [];
        $query = '
        SELECT
            u.user_id,
            u.first_name,
            u.last_name,
            COUNT(DISTINCT a.album_id) AS `albums_count`,
            COUNT(DISTINCT p.photo_id) AS `photos_count`
        FROM user u
        LEFT JOIN album a ON u.user_id = a.user_id
        LEFT JOIN photo p ON a.album_id = p.album_id
        GROUP BY u.user_id';

        if (null !== $offset && null !== $limit) {
            $query .= ' LIMIT :offset, :limit';

            $parameters['offset'] = $offset;
            $parameters['limit'] = $limit;
        }

        return $this->storage->fetchAll($query, $parameters);
    }

    /**
     * @param int $userId
     * @param string $firstName
     * @param string $lastName
     */
    public function saveUser($userId, $firstName, $lastName)
    {
        $this->storage->insert('INSERT INTO `user` VALUES (:userId, :firstName, :lastName)', [
            'userId' => $userId,
            'firstName' => $firstName,
            'lastName' => $lastName
        ]);
    }
}
