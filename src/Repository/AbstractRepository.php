<?php

namespace VKPhotoParser\Repository;

use VKPhotoParser\Storage\StorageInterface;

class AbstractRepository
{
    /**
     * @var StorageInterface
     */
    protected $storage;

    /**
     * @param StorageInterface $storage
     */
    public function __construct(StorageInterface $storage)
    {
        $this->storage = $storage;
    }
}