<?php

namespace VKPhotoParser;

class HttpClient implements HttpClientInterface
{
    /**
     * @param string $url
     * @return string
     */
    public function sendRequestAndReturnResponse($url)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_URL, $url);
        $output = curl_exec($ch);

        curl_close($ch);

        return $output;
    }
}
