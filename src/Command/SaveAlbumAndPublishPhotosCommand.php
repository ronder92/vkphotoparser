<?php

namespace VKPhotoParser\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

use VKPhotoParser\Amqp\Consumer\AlbumConsumer;

class SaveAlbumAndPublishPhotosCommand extends Command
{
    /**
     * @var AlbumConsumer
     */
    private $albumConsumer;

    /**
     * @param AlbumConsumer $albumConsumer
     * @param string|null $name
     */
    public function __construct(AlbumConsumer $albumConsumer, $name = null)
    {
        $this->albumConsumer = $albumConsumer;

        parent::__construct($name);
    }

    /**
     * @inheritdoc
     */
    protected function configure()
    {
        $this->setName('vk:consumer-save-album-and-publish-photos');
    }

    /**
     * @inheritdoc
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->albumConsumer->setConsumerTag('album');
        $this->albumConsumer->consume();
    }
}
