<?php

namespace VKPhotoParser\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

use VKPhotoParser\Repository\UserRepository;

class GetStatCommand extends Command
{
    const DEFAULT_USERS_DISPLAY_OFFSET = 0;
    const DEFAULT_USERS_DISPLAY_LIMIT = 10;

    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * @param UserRepository $userRepository
     * @param null|string $name
     */
    public function __construct(UserRepository $userRepository, $name = null)
    {
        $this->userRepository = $userRepository;

        parent::__construct($name);
    }

    /**
     * @inheritdoc
     */
    protected function configure()
    {
        $this
            ->setName('vk:get-stat')
            ->setDescription('Display users with count of albums and photos')
            ->addOption('offset', null, InputOption::VALUE_OPTIONAL, '', self::DEFAULT_USERS_DISPLAY_OFFSET)
            ->addOption('limit', null, InputOption::VALUE_OPTIONAL, '', self::DEFAULT_USERS_DISPLAY_LIMIT)
        ;
    }

    /**
     * @inheritdoc
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $offset = $input->getOption('offset');
        $limit = $input->getOption('limit');

        if (!is_numeric($offset) || !is_numeric($limit)) {
            throw new \InvalidArgumentException('Offset and limit should be numeric');
        }

        $usersWithAlbumsAndPhotosCount = $this->userRepository->findAllUserWithAlbumsAndPhotosCount(
            (int)$offset,
            (int)$limit
        );

        $symfonyStyle = new SymfonyStyle($input, $output);
        $symfonyStyle->title('Photos and albums of users');

        $symfonyStyle->table(
            ['User ID', 'First name', 'Last name', 'Albums', 'Photos'],
            $usersWithAlbumsAndPhotosCount
        );
    }
}
