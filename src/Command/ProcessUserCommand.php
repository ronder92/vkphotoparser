<?php

namespace VKPhotoParser\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

use VKPhotoParser\Amqp\Producer\Producer;
use VKPhotoParser\Repository\UserRepository;
use VKPhotoParser\VkApi;

class ProcessUserCommand extends Command
{
    /**
     * @var VkApi
     */
    private $vkApi;

    /**
     * @var Producer
     */
    private $userProducer;

    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * @param UserRepository $photoConsumer
     * @param VkApi $vkApi
     * @param Producer $userProducer
     * @param string|null $name
     */
    public function __construct(UserRepository $photoConsumer, VkApi $vkApi, Producer $userProducer, $name = null)
    {
        $this->userRepository = $photoConsumer;
        $this->vkApi = $vkApi;
        $this->userProducer = $userProducer;

        parent::__construct($name);
    }

    /**
     * @inheritdoc
     */
    protected function configure()
    {
        $this
            ->setName('vk:process-user')
            ->setDescription('Parse albums and photos of user')
            ->addArgument('userId', InputArgument::REQUIRED)
        ;
    }

    /**
     * @inheritdoc
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $userId = $input->getArgument('userId');

        $symfonyStyle = new SymfonyStyle($input, $output);

        $symfonyStyle->title(
            sprintf('Process user #%d', $userId)
        );

        $user = $this->vkApi->getUserById($userId);
        $this->userProducer->publish([
            'uid' => $user['uid'],
            'first_name' => $user['first_name'],
            'last_name' => $user['last_name']
        ]);
    }
}
