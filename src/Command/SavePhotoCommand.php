<?php

namespace VKPhotoParser\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

use VKPhotoParser\Amqp\Consumer\PhotoConsumer;

class SavePhotoCommand extends Command
{
    /**
     * @var PhotoConsumer
     */
    private $photoConsumer;

    /**
     * @param PhotoConsumer $photoConsumer
     * @param string|null $name
     */
    public function __construct(PhotoConsumer $photoConsumer, $name = null)
    {
        $this->photoConsumer = $photoConsumer;

        parent::__construct($name);
    }

    /**
     * @inheritdoc
     */
    protected function configure()
    {
        $this->setName('vk:consumer-save-photo');
    }

    /**
     * @inheritdoc
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->photoConsumer->setConsumerTag('photo');
        $this->photoConsumer->consume();
    }
}
