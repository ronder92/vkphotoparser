<?php

namespace VKPhotoParser\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

use VKPhotoParser\Repository\AlbumRepository;
use VKPhotoParser\Repository\PhotoRepository;
use VKPhotoParser\Repository\UserRepository;
use VKPhotoParser\VkApi;

class GetUserStatCommand extends Command
{
    /**
     * @var AlbumRepository
     */
    private $albumRepository;

    /**
     * @var PhotoRepository
     */
    private $photoRepository;

    /**
     * @var UserRepository
     */
    private $userRepository;
    /**
     * @var VkApi
     */
    private $vkApi;

    /**
     * @param AlbumRepository $albumRepository
     * @param PhotoRepository $photoRepository
     * @param UserRepository $userRepository
     * @param VkApi $vkApi
     * @param string|null $name
     */
    public function __construct(
        AlbumRepository $albumRepository,
        PhotoRepository $photoRepository,
        UserRepository $userRepository,
        VkApi $vkApi,
        $name = null
    ) {
        $this->albumRepository = $albumRepository;
        $this->photoRepository = $photoRepository;
        $this->userRepository = $userRepository;
        $this->vkApi = $vkApi;

        parent::__construct($name);
    }

    /**
     * @inheritdoc
     */
    protected function configure()
    {
        $this
            ->setName('vk:get-user-stat')
            ->setDescription('Display user albums and photos')
            ->addArgument('userId', InputArgument::REQUIRED)
        ;
    }

    /**
     * @inheritdoc
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $userId = $input->getArgument('userId');

        if (!is_numeric($userId)) {
            throw new \InvalidArgumentException('Invalid userId');
        }

        if ($this->userRepository->countByUserId($userId) == 0) {
            throw new  \InvalidArgumentException(
                sprintf('User with ID #%d is not found', $userId)
            );
        }

        $userAlbums = $this->albumRepository->findByUserId($userId);
        $userPhotos = $this->photoRepository->getPhotosWithAlbumsByUserId($userId);

        $this->vkApi->addDomainToPhotoPath($userPhotos);

        $symfonyStyle = new SymfonyStyle($input, $output);
        $symfonyStyle->title(sprintf('Photos and albums of user #%d', $userId));
        $symfonyStyle->table(['Album ID', 'Name'], $userAlbums);
        $symfonyStyle->table(['Photo ID', 'Album ID', 'Album name', 'Url'], $userPhotos);
    }
}
