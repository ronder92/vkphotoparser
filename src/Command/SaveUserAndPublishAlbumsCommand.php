<?php

namespace VKPhotoParser\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

use VKPhotoParser\Amqp\Consumer\UserConsumer;

class SaveUserAndPublishAlbumsCommand extends Command
{
    /**
     * @var UserConsumer
     */
    private $userConsumer;

    /**
     * @param UserConsumer $photoConsumer
     * @param string|null $name
     */
    public function __construct(UserConsumer $photoConsumer, $name = null)
    {
        $this->userConsumer = $photoConsumer;

        parent::__construct($name);
    }

    /**
     * @inheritdoc
     */
    protected function configure()
    {
        $this->setName('vk:consumer-save-user-and-publish-albums');
    }

    /**
     * @inheritdoc
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->userConsumer->setConsumerTag('user');
        $this->userConsumer->consume();
    }
}
