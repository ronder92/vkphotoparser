<?php

namespace VKPhotoParser;

use VKPhotoParser\Amqp\Consumer\AlbumConsumer;
use VKPhotoParser\Amqp\AmqpConnection;
use VKPhotoParser\Amqp\Consumer\PhotoConsumer;
use VKPhotoParser\Amqp\Consumer\UserConsumer;
use VKPhotoParser\Amqp\Exchange;
use VKPhotoParser\Amqp\Queue;
use VKPhotoParser\Amqp\Producer\Producer;
use VKPhotoParser\Repository\AlbumRepository;
use VKPhotoParser\Repository\PhotoRepository;
use VKPhotoParser\Repository\UserRepository;
use VKPhotoParser\Storage\MysqlStorage;

class Container
{
    /**
     * @var array
     */
    private $parameters = [];

    /**
     * @var array
     */
    static private $services;

    /**
     * @param array $parameters
     */
    public function __construct(array $parameters)
    {
        $this->parameters = $parameters;
    }

    /**
     * @return AmqpConnection
     */
    public function getAmqpConnection()
    {
        if (isset(self::$services[AmqpConnection::class])) {
            return self::$services[AmqpConnection::class];
        }

        return self::$services[AmqpConnection::class] =
            new AmqpConnection(
                $this->parameters['amqp.host'],
                $this->parameters['amqp.port'],
                $this->parameters['amqp.user'],
                $this->parameters['amqp.password']
            );
    }

    /**
     * @return Producer
     */
    public function getUserProducer()
    {
        $userProducerServiceName = 'user_producer';

        if (isset(self::$services[$userProducerServiceName])) {
            return self::$services[$userProducerServiceName];
        }

        return self::$services[$userProducerServiceName] = new Producer(
            $this->getAmqpConnection(),
            $this->getUserExchange(),
            $this->getUserQueue()
        );
    }

    /**
     * @return Producer
     */
    public function getAlbumProducer()
    {
        $albumProducerServiceName = 'album_producer';

        if (isset(self::$services[$albumProducerServiceName])) {
            return self::$services[$albumProducerServiceName];
        }

        return self::$services[$albumProducerServiceName] = new Producer(
            $this->getAmqpConnection(),
            $this->getAlbumExchange(),
            $this->getAlbumQueue()
        );
    }

    /**
     * @return Producer
     */
    public function getPhotoProducer()
    {
        $photoProducerServiceName = 'photo_producer';

        if (isset(self::$services[$photoProducerServiceName])) {
            return self::$services[$photoProducerServiceName];
        }

        return self::$services[$photoProducerServiceName] = new Producer(
            $this->getAmqpConnection(),
            $this->getPhotoExchange(),
            $this->getPhotoQueue()
        );
    }

    /**
     * @return MysqlStorage
     */
    public function getMysqlStorage()
    {
        if (isset(self::$services[MysqlStorage::class])) {
            return self::$services[MysqlStorage::class];
        }

        return self::$services[MysqlStorage::class] = new MysqlStorage(
            $this->parameters['mysql.host'],
            $this->parameters['mysql.user'],
            $this->parameters['mysql.password'],
            $this->parameters['mysql.db']
        );
    }

    /**
     * @return UserRepository
     */
    public function getUserRepository()
    {
        if (isset(self::$services[UserRepository::class])) {
            return self::$services[UserRepository::class];
        }

        return self::$services[UserRepository::class] = new UserRepository($this->getMysqlStorage());
    }

    /**
     * @return AlbumRepository
     */
    public function getAlbumRepository()
    {
        if (isset(self::$services[AlbumRepository::class])) {
            return self::$services[AlbumRepository::class];
        }

        return self::$services[AlbumRepository::class] = new AlbumRepository($this->getMysqlStorage());
    }

    /**
     * @return PhotoRepository
     */
    public function getPhotoRepository()
    {
        if (isset(self::$services[PhotoRepository::class])) {
            return self::$services[PhotoRepository::class];
        }

        return self::$services[PhotoRepository::class] = new PhotoRepository($this->getMysqlStorage());
    }

    /**
     * @return HttpClient
     */
    public function getHttpClient()
    {
        if (isset(self::$services[HttpClient::class])) {
            return self::$services[HttpClient::class];
        }

        return self::$services[HttpClient::class] = new HttpClient();
    }

    /**
     * @return VkApi
     */
    public function getVkApi()
    {
        if (isset(self::$services[VkApi::class])) {
            return self::$services[VkApi::class];
        }

        return self::$services[VkApi::class] = new VkApi($this->getHttpClient());
    }

    /**
     * @return UserConsumer
     */
    public function getUserConsumer()
    {
        if (isset(self::$services[UserConsumer::class])) {
            return self::$services[UserConsumer::class];
        }

        return self::$services[UserConsumer::class] = new UserConsumer(
            $this->getAmqpConnection(),
            $this->getUserExchange(),
            $this->getUserQueue(),
            $this->getUserRepository(),
            $this->getVkApi(),
            $this->getAlbumProducer()
        );
    }

    /**
     * @return AlbumConsumer
     */
    public function getAlbumConsumer()
    {
        if (isset(self::$services[AlbumConsumer::class])) {
            return self::$services[AlbumConsumer::class];
        }

        return self::$services[AlbumConsumer::class] = new AlbumConsumer(
            $this->getAmqpConnection(),
            $this->getAlbumExchange(),
            $this->getAlbumQueue(),
            $this->getAlbumRepository(),
            $this->getVkApi(),
            $this->getPhotoProducer()
        );
    }

    /**
     * @return PhotoConsumer
     */
    public function getPhotoConsumer()
    {
        if (isset(self::$services[PhotoConsumer::class])) {
            return self::$services[PhotoConsumer::class];
        }

        return self::$services[PhotoConsumer::class] = new PhotoConsumer(
            $this->getAmqpConnection(),
            $this->getPhotoExchange(),
            $this->getPhotoQueue(),
            $this->getPhotoRepository(),
            $this->getVkApi()
        );
    }

    /**
     * @return Exchange
     */
    protected function getUserExchange()
    {
        $userExchangeServiceName = 'user_exchange';

        if (isset(self::$services[$userExchangeServiceName])) {
            return self::$services[$userExchangeServiceName];
        }

        return self::$services[$userExchangeServiceName] = new Exchange($this->parameters['amqp.exchange.user.name']);
    }

    /**
     * @return Exchange
     */
    protected function getAlbumExchange()
    {
        $photoExchangeServiceName = 'album_exchange';

        if (isset(self::$services[$photoExchangeServiceName])) {
            return self::$services[$photoExchangeServiceName];
        }

        return self::$services[$photoExchangeServiceName] = new Exchange($this->parameters['amqp.exchange.album.name']);
    }

    /**
     * @return Exchange
     */
    protected function getPhotoExchange()
    {
        $photoExchangeServiceName = 'photo_exchange';

        if (isset(self::$services[$photoExchangeServiceName])) {
            return self::$services[$photoExchangeServiceName];
        }

        return self::$services[$photoExchangeServiceName] = new Exchange($this->parameters['amqp.exchange.photo.name']);
    }

    /**
     * @return Queue
     */
    protected function getUserQueue()
    {
        $userQueueServiceName = 'user_queue';

        if (isset(self::$services[$userQueueServiceName])) {
            return self::$services[$userQueueServiceName];
        }

        return self::$services[$userQueueServiceName] = new Queue($this->parameters['amqp.queue.user.name']);
    }

    /**
     * @return Queue
     */
    protected function getAlbumQueue()
    {
        $albumQueueServiceName = 'album_queue';

        if (isset(self::$services[$albumQueueServiceName])) {
            return self::$services[$albumQueueServiceName];
        }

        return self::$services[$albumQueueServiceName] = new Queue($this->parameters['amqp.queue.album.name']);
    }

    /**
     * @return Queue
     */
    protected function getPhotoQueue()
    {
        $photoQueueServiceName = 'photo_queue';

        if (isset(self::$services[$photoQueueServiceName])) {
            return self::$services[$photoQueueServiceName];
        }

        return self::$services[$photoQueueServiceName] = new Queue($this->parameters['amqp.queue.photo.name']);
    }
}
