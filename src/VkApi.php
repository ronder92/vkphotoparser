<?php

namespace VKPhotoParser;

use VKPhotoParser\Exception\VkApiException;

class VkApi
{
    const API_DOMAIN = 'api.vk.com';
    const PHOTOS_DOMAIN = 'pp.userapi.com';

    /**
     * @var HttpClient
     */
    private $httpClient;

    /**
     * @param HttpClientInterface $httpClient
     */
    public function __construct(HttpClientInterface $httpClient)
    {
        $this->httpClient = $httpClient;
    }

    /**
     * @param int $userId
     * @return array
     * @throws VkApiException
     */
    public function getUserById($userId)
    {
        $userResponse = $this->getParsedResponse('users.get', [
            'user_ids' => $userId
        ]);

        return array_shift($userResponse['response']);
    }

    /**
     * @param int $userId
     * @param int $albumId
     * @return array
     * @throws VkApiException
     */
    public function getPhotosByAlbumId($userId, $albumId)
    {
        $photosResponse = $this->getParsedResponse('photos.get', [
            'owner_id' => $userId,
            'album_id' => $albumId,
            'photo_sizes' => 1
        ]);

        return $photosResponse['response'];
    }

    /**
     * @param int $userId
     * @return array
     * @throws VkApiException
     */
    public function getAlbumsByUserId($userId)
    {
        $albumsResponse = $this->getParsedResponse('photos.getAlbums', [
            'owner_id' => $userId
        ]);

        return $albumsResponse['response'];
    }

    /**
     * @param array $userPhotos
     */
    public function addDomainToPhotoPath(array &$userPhotos)
    {
        array_walk($userPhotos, function (&$userPhoto) {
            $userPhoto['path'] = $this->buildUrlToPhoto($userPhoto['path']);
        });
    }

    /**
     * @param string $method
     * @param array $params
     * @return string
     */
    private function buildUrlToApi($method, $params)
    {
        return 'https://' . self::API_DOMAIN . '/method/' . $method . '?' . http_build_query($params);
    }

    /**
     * @param string $path
     * @return string
     */
    private function buildUrlToPhoto($path)
    {
        return 'https://' . self::PHOTOS_DOMAIN . $path;
    }

    /**
     * @param string $method
     * @param array $params
     * @return array
     * @throws VkApiException
     */
    private function getParsedResponse($method, $params)
    {
        $response = $this->httpClient->sendRequestAndReturnResponse($this->buildUrlToApi($method, $params));
        $jsonDecodedResponse = json_decode($response, true);

        if (isset($albumsResponse['error'])) {
            throw new VkApiException($albumsResponse['error']['error_msg']);
        }

        return $jsonDecodedResponse;
    }
}
