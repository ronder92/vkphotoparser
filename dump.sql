CREATE TABLE `user` (
  `user_id` INT UNSIGNED NOT NULL,
  `first_name` VARCHAR(255) NOT NULL,
  `last_name` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`user_id`));

CREATE TABLE `album` (
  `album_id` INT UNSIGNED NOT NULL,
  `user_id` INT UNSIGNED NOT NULL,
  `name` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`album_id`),
  INDEX `INDEX_user_id` (`user_id` ASC),
  CONSTRAINT `FK_ALBUM_TO_USER`
  FOREIGN KEY (`user_id`)
  REFERENCES `user` (`user_id`)
    ON DELETE RESTRICT
    ON UPDATE RESTRICT);

CREATE TABLE `photo` (
  `photo_id` INT UNSIGNED NOT NULL,
  `album_id` INT UNSIGNED NOT NULL,
  `path` TINYTEXT NOT NULL,
  PRIMARY KEY (`photo_id`),
  INDEX `INDEX_album_id` (`album_id` ASC),
  CONSTRAINT `FK_PHOTO_TO_ALBUM`
  FOREIGN KEY (`album_id`)
  REFERENCES `album` (`album_id`)
    ON DELETE RESTRICT
    ON UPDATE RESTRICT);
